const express = require('express');
const app = express();

const mongoose = require("mongoose");

mongoose
    .connect(
        `mongodb://mongodb-service/car`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("MongoDB successfully connected10"))
    .catch(err => console.log(err));

app.get('/', (req, res) => {
    res.end(`Hi this is car app, PID: ${process.pid}`);
});

app.listen(8080);

console.log(`Server running on 8080 port, PID: ${process.pid}`);